package ru.t1.skasabov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.client.AuthRestEndpointClient;
import ru.t1.skasabov.tm.client.ProjectRestEndpointClient;
import ru.t1.skasabov.tm.client.TaskRestEndpointClient;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.marker.IntegrationCategory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProjectRestEndpointClientTest {

    private final static int NUMBER_OF_ENTITIES = 4;

    @NotNull
    private String userId;

    @NotNull
    private final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    private final TaskRestEndpointClient taskClient = TaskRestEndpointClient.client();

    @NotNull
    private final AuthRestEndpointClient authClient = AuthRestEndpointClient.client();

    @NotNull
    private List<ProjectDto> projectList = new ArrayList<>();

    @NotNull
    private List<TaskDto> taskList = new ArrayList<>();

    @NotNull
    private final ProjectDto project1 = new ProjectDto("Test Project 1");

    @NotNull
    private final ProjectDto project2 = new ProjectDto("Test Project 2");

    @NotNull
    private final ProjectDto project3 = new ProjectDto("Test Project 3");

    @NotNull
    private final ProjectDto project4 = new ProjectDto("Test Project 4");

    @Before
    public void initTest() {
        authClient.login("test", "test");
        @Nullable final UserDto user = authClient.profile();
        Assert.assertNotNull(user);
        userId = user.getId();
        projectList = client.findAll();
        taskList = taskClient.findAll();
        client.clear();
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        project4.setUserId(userId);
        client.save(project1);
        client.save(project2);
        client.save(project3);
        client.save(project4);
        @NotNull final TaskDto task1 = new TaskDto("Test Task 1");
        task1.setProjectId(project1.getId());
        task1.setUserId(userId);
        @NotNull final TaskDto task2 = new TaskDto("Test Task 2");
        task2.setProjectId(project2.getId());
        task2.setUserId(userId);
        @NotNull final TaskDto task3 = new TaskDto("Test Task 3");
        task3.setProjectId(project3.getId());
        task3.setUserId(userId);
        @NotNull final TaskDto task4 = new TaskDto("Test Task 4");
        task4.setProjectId(project4.getId());
        task4.setUserId(userId);
        taskClient.save(task1);
        taskClient.save(task2);
        taskClient.save(task3);
        taskClient.save(task4);
    }

    @After
    public void clean() {
        client.clear();
        for (@NotNull final TaskDto task : taskList) taskClient.save(task);
        for (@NotNull final ProjectDto project : projectList) client.save(project);
        authClient.logout();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testFindAll() {
        @NotNull final List<ProjectDto> projects = client.findAll();
        Assert.assertEquals(NUMBER_OF_ENTITIES, projects.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testSave() {
        @NotNull final ProjectDto project = new ProjectDto("Test Project");
        project.setUserId(userId);
        client.save(project);
        Assert.assertEquals(NUMBER_OF_ENTITIES + 1, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testFindById() {
        @Nullable final ProjectDto project = client.findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals("Test Project 1", project.getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testFindByInvalidId() {
        @Nullable final ProjectDto project = client.findById("123");
        Assert.assertNull(project);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testExistsById() {
        boolean existsProject = client.existsById(project1.getId());
        Assert.assertTrue(existsProject);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testExistsByInvalidId() {
        boolean existsProject = client.existsById("123");
        Assert.assertFalse(existsProject);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testCount() {
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testDeleteById() {
        client.deleteById(project1.getId());
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testDelete() {
        client.delete(project1);
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testDeleteProjects() {
        @NotNull final List<ProjectDto> projects = new ArrayList<>();
        projects.add(project2);
        projects.add(project3);
        projects.add(project4);
        client.deleteAll(projects);
        Assert.assertEquals(NUMBER_OF_ENTITIES - projects.size(), client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testDeleteEmptyProjects() {
        client.deleteAll(Collections.emptyList());
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testClear() {
        client.clear();
        Assert.assertEquals(0, client.count());
    }

}

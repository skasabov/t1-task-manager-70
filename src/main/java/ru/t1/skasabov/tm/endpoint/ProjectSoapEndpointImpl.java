package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.dto.soap.*;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;
import ru.t1.skasabov.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public final class ProjectSoapEndpointImpl {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://skasabov.t1.ru/tm/dto/soap";

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    private ProjectDto convertToDto(@NotNull final ru.t1.skasabov.tm.dto.soap.ProjectDto project) {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setCreated(project.getCreated());
        projectDto.setUserId(project.getUserId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setDateStart(project.getDateStart());
        projectDto.setDateFinish(project.getDateFinish());
        projectDto.setStatus(Status.valueOf(project.getStatus().value()));
        return projectDto;
    }

    @NotNull
    private List<ProjectDto> convertToCollectionDto(@NotNull final List<ru.t1.skasabov.tm.dto.soap.ProjectDto> projects) {
        @NotNull final List<ProjectDto> projectsDto = new ArrayList<>();
        for (@NotNull final ru.t1.skasabov.tm.dto.soap.ProjectDto project : projects) {
            projectsDto.add(convertToDto(project));
        }
        return projectsDto;
    }

    @NotNull
    private ru.t1.skasabov.tm.dto.soap.ProjectDto convertFromDto(@NotNull final ProjectDto projectDto) {
        @NotNull final ru.t1.skasabov.tm.dto.soap.ProjectDto project = new ru.t1.skasabov.tm.dto.soap.ProjectDto();
        project.setId(projectDto.getId());
        project.setCreated(projectDto.getCreated());
        project.setUserId(projectDto.getUserId());
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setDateStart(projectDto.getDateStart());
        project.setDateFinish(projectDto.getDateFinish());
        project.setStatus(ru.t1.skasabov.tm.dto.soap.Status.fromValue(projectDto.getStatus().name()));
        return project;
    }

    @Nullable
    private ru.t1.skasabov.tm.dto.soap.ProjectDto convertFromNullableDto(@Nullable final ProjectDto projectDto) {
        if (projectDto == null) return null;
        @NotNull final ru.t1.skasabov.tm.dto.soap.ProjectDto project = new ru.t1.skasabov.tm.dto.soap.ProjectDto();
        project.setId(projectDto.getId());
        project.setCreated(projectDto.getCreated());
        projectDto.setUserId(project.getUserId());
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setDateStart(projectDto.getDateStart());
        project.setDateFinish(projectDto.getDateFinish());
        project.setStatus(ru.t1.skasabov.tm.dto.soap.Status.fromValue(projectDto.getStatus().name()));
        return project;
    }

    @NotNull
    private List<ru.t1.skasabov.tm.dto.soap.ProjectDto> convertFromCollectionDto(@NotNull final List<ProjectDto> projectsDto) {
        @NotNull final List<ru.t1.skasabov.tm.dto.soap.ProjectDto> projects = new ArrayList<>();
        for (@NotNull final ProjectDto projectDto : projectsDto) {
            projects.add(convertFromDto(projectDto));
        }
        return projects;
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@NotNull @RequestPayload final ProjectFindAllRequest request) {
        return new ProjectFindAllResponse(convertFromCollectionDto(projectRepository.findAllByUserId(UserUtil.getUserId())));
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@NotNull @RequestPayload final ProjectSaveRequest request) {
        @NotNull final ProjectDto project = convertToDto(request.getProject());
        project.setUserId(UserUtil.getUserId());
        return new ProjectSaveResponse(convertFromDto(projectRepository.save(project)));
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@NotNull @RequestPayload final ProjectFindByIdRequest request) {
        return new ProjectFindByIdResponse(convertFromNullableDto(projectRepository.findByUserIdAndId(UserUtil.getUserId(), request.getId())));
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse existsById(@NotNull @RequestPayload final ProjectExistsByIdRequest request) {
        return new ProjectExistsByIdResponse(projectRepository.existsByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@NotNull @RequestPayload final ProjectCountRequest request) {
        return new ProjectCountResponse(projectRepository.countByUserId(UserUtil.getUserId()));
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@NotNull @RequestPayload final ProjectDeleteByIdRequest request) {
        @NotNull final String id = request.getId();
        taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), id);
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), id);
        return new ProjectDeleteByIdResponse();
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(@NotNull @RequestPayload final ProjectDeleteRequest request) {
        @NotNull final ProjectDto project = convertToDto(request.getProject());
        @NotNull final String id = project.getId();
        taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), id);
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), id);
        return new ProjectDeleteResponse();
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse deleteAll(@NotNull @RequestPayload final ProjectDeleteAllRequest request) {
        @NotNull final List<ProjectDto> projects = convertToCollectionDto(request.getProjects());
        for (@NotNull final ProjectDto project : projects) {
            taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), project.getId());
            projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project.getId());
        }
        return new ProjectDeleteAllResponse();
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@NotNull @RequestPayload final ProjectClearRequest request) {
        @NotNull final List<ProjectDto> projects = projectRepository.findAll();
        for (@NotNull final ProjectDto project : projects) taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), project.getId());
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
        return new ProjectClearResponse();
    }

}

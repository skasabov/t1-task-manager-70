package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.dto.soap.*;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;
import ru.t1.skasabov.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public final class TaskSoapEndpointImpl {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://skasabov.t1.ru/tm/dto/soap";

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    private TaskDto convertToDto(@NotNull final ru.t1.skasabov.tm.dto.soap.TaskDto task) {
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setCreated(task.getCreated());
        taskDto.setUserId(task.getUserId());
        taskDto.setName(task.getName());
        taskDto.setDescription(task.getDescription());
        taskDto.setDateStart(task.getDateStart());
        taskDto.setDateFinish(task.getDateFinish());
        taskDto.setStatus(ru.t1.skasabov.tm.enumerated.Status.valueOf(task.getStatus().value()));
        return taskDto;
    }

    @NotNull
    private List<TaskDto> convertToCollectionDto(@NotNull final List<ru.t1.skasabov.tm.dto.soap.TaskDto> tasks) {
        @NotNull final List<TaskDto> tasksDto = new ArrayList<>();
        for (@NotNull final ru.t1.skasabov.tm.dto.soap.TaskDto task : tasks) {
            tasksDto.add(convertToDto(task));
        }
        return tasksDto;
    }

    @NotNull
    private ru.t1.skasabov.tm.dto.soap.TaskDto convertFromDto(@NotNull final TaskDto taskDto) {
        @NotNull final ru.t1.skasabov.tm.dto.soap.TaskDto task = new ru.t1.skasabov.tm.dto.soap.TaskDto();
        task.setId(taskDto.getId());
        task.setCreated(taskDto.getCreated());
        task.setUserId(taskDto.getUserId());
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setDateStart(taskDto.getDateStart());
        task.setDateFinish(taskDto.getDateFinish());
        task.setStatus(ru.t1.skasabov.tm.dto.soap.Status.fromValue(taskDto.getStatus().name()));
        return task;
    }

    @Nullable
    private ru.t1.skasabov.tm.dto.soap.TaskDto convertFromNullableDto(@Nullable final TaskDto taskDto) {
        if (taskDto == null) return null;
        @NotNull final ru.t1.skasabov.tm.dto.soap.TaskDto task = new ru.t1.skasabov.tm.dto.soap.TaskDto();
        task.setId(taskDto.getId());
        task.setCreated(taskDto.getCreated());
        taskDto.setUserId(task.getUserId());
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setDateStart(taskDto.getDateStart());
        task.setDateFinish(taskDto.getDateFinish());
        task.setStatus(ru.t1.skasabov.tm.dto.soap.Status.fromValue(taskDto.getStatus().name()));
        return task;
    }

    @NotNull
    private List<ru.t1.skasabov.tm.dto.soap.TaskDto> convertFromCollectionDto(@NotNull final List<TaskDto> tasksDto) {
        @NotNull final List<ru.t1.skasabov.tm.dto.soap.TaskDto> tasks = new ArrayList<>();
        for (@NotNull final TaskDto taskDto : tasksDto) {
            tasks.add(convertFromDto(taskDto));
        }
        return tasks;
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@NotNull @RequestPayload final TaskFindAllRequest request) {
        return new TaskFindAllResponse(convertFromCollectionDto(taskRepository.findAllByUserId(UserUtil.getUserId())));
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(@NotNull @RequestPayload final TaskSaveRequest request) {
        @NotNull final TaskDto task = convertToDto(request.getTask());
        task.setUserId(UserUtil.getUserId());
        return new TaskSaveResponse(convertFromDto(taskRepository.save(task)));
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@NotNull @RequestPayload final TaskFindByIdRequest request) {
        return new TaskFindByIdResponse(convertFromNullableDto(taskRepository.findByUserIdAndId(UserUtil.getUserId(), request.getId())));
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = NAMESPACE)
    public TaskExistsByIdResponse existsById(@NotNull @RequestPayload final TaskExistsByIdRequest request) {
        return new TaskExistsByIdResponse(taskRepository.existsByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(@NotNull @RequestPayload final TaskCountRequest request) {
        return new TaskCountResponse(taskRepository.countByUserId(UserUtil.getUserId()));
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@NotNull @RequestPayload final TaskDeleteByIdRequest request) {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), request.getId());
        return new TaskDeleteByIdResponse();
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse delete(@NotNull @RequestPayload final TaskDeleteRequest request) {
        @NotNull final TaskDto task = convertToDto(request.getTask());
        @NotNull final String id = task.getId();
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), id);
        return new TaskDeleteResponse();
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse deleteAll(@NotNull @RequestPayload final TaskDeleteAllRequest request) {
        @NotNull final List<TaskDto> tasks = convertToCollectionDto(request.getTasks());
        for (@NotNull final TaskDto task : tasks) {
            taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task.getId());
        }
        return new TaskDeleteAllResponse();
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse clear(@NotNull @RequestPayload final TaskClearRequest request) {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
        return new TaskClearResponse();
    }

}

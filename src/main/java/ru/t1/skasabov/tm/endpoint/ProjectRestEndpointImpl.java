package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.skasabov.tm.api.IProjectRestEndpoint;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;
import ru.t1.skasabov.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public final class ProjectRestEndpointImpl implements IProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Override
    @GetMapping("/findAll")
    public List<ProjectDto> findAll() {
        return projectRepository.findAllByUserId(UserUtil.getUserId());
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public ProjectDto save(
            @NotNull @RequestBody final ProjectDto project
    ) {
        project.setUserId(UserUtil.getUserId());
        return projectRepository.save(project);
    }

    @NotNull
    @Override
    @GetMapping("/findById/{id}")
    public ProjectDto findById(
            @NotNull @PathVariable("id") final String id
    ) {
        return projectRepository.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull @PathVariable("id") final String id
    ) {
        return projectRepository.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectRepository.countByUserId(UserUtil.getUserId());
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull @PathVariable("id") final String id
    ) {
        taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), id);
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(
            @NotNull @RequestBody final ProjectDto project
    ) {
        taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), project.getId());
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project.getId());
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull @RequestBody final List<ProjectDto> projects
    ) {
        for (@NotNull final ProjectDto project : projects) {
            taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), project.getId());
            projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project.getId());
        }
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        @NotNull final List<ProjectDto> projects = projectRepository.findAllByUserId(UserUtil.getUserId());
        for (@NotNull final ProjectDto project : projects) taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), project.getId());
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
    }

}

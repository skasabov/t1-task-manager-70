package ru.t1.skasabov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.skasabov.tm")
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.t1.skasabov.tm.repository")
public class DatabaseConfiguration {

    @Bean
    @NotNull
    public DataSource dataSource(
            @NotNull @Value("#{environment['database.driver']}") final String databaseDriver,
            @NotNull @Value("#{environment['database.url']}") final String databaseUrl,
            @NotNull @Value("#{environment['database.username']}") final String databaseUsername,
            @NotNull @Value("#{environment['database.password']}") final String databasePassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseDriver);
        dataSource.setUrl(databaseUrl);
        dataSource.setUsername(databaseUsername);
        dataSource.setPassword(databasePassword);
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @NotNull @Value("#{environment['database.dialect']}") final String databaseDialect,
            @NotNull @Value("#{environment['database.hbm2ddl_auto']}") final String databaseHbm2ddlAuto,
            @NotNull @Value("#{environment['database.show_sql']}") final String databaseShowSql
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.skasabov.tm.dto", "ru.t1.skasabov.tm.model");
        @NotNull final Properties properties = new Properties();
        properties.put("hibernate.show_sql", databaseShowSql);
        properties.put("hibernate.hbm2ddl.auto", databaseHbm2ddlAuto);
        properties.put("hibernate.dialect", databaseDialect);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}

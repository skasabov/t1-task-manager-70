package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "result", propOrder = {
        "success",
        "message"
})
public class Result implements Serializable {

    @XmlElement(required = true)
    private boolean success;

    @XmlElement
    private String message;

    public Result(final boolean success) {
        this.success = success;
    }

    public Result(final Exception e) {
        this.message = e.getMessage();
    }

}

package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "project"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectSaveResponse")
public class ProjectSaveResponse {

    @NotNull
    @XmlElement(name = "project")
    protected ProjectDto project;

    public ProjectSaveResponse(@NotNull final ProjectDto project) {
        this.project = project;
    }

}

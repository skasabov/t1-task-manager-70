package ru.t1.skasabov.tm.dto.soap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
@NoArgsConstructor
public class ObjectFactory {

    @NotNull
    public ProjectClearRequest createProjectClearRequest() {
        return new ProjectClearRequest();
    }

    @NotNull
    public ProjectClearResponse createProjectClearResponse() {
        return new ProjectClearResponse();
    }

    @NotNull
    public ProjectCountRequest createProjectCountRequest() {
        return new ProjectCountRequest();
    }

    @NotNull
    public ProjectCountResponse createProjectCountResponse() {
        return new ProjectCountResponse();
    }

    @NotNull
    public ProjectDeleteAllRequest createProjectDeleteAllRequest() {
        return new ProjectDeleteAllRequest();
    }

    @NotNull
    public ProjectDeleteAllResponse createProjectDeleteAllResponse() {
        return new ProjectDeleteAllResponse();
    }

    @NotNull
    public ProjectDeleteByIdRequest createProjectDeleteByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    @NotNull
    public ProjectDeleteByIdResponse createProjectDeleteByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    @NotNull
    public ProjectDeleteRequest createProjectDeleteRequest() {
        return new ProjectDeleteRequest();
    }

    @NotNull
    public ProjectDeleteResponse createProjectDeleteResponse() {
        return new ProjectDeleteResponse();
    }

    @NotNull
    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    @NotNull
    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    @NotNull
    public ProjectExistsByIdRequest createProjectExistsByIdRequest() {
        return new ProjectExistsByIdRequest();
    }

    @NotNull
    public ProjectExistsByIdResponse createProjectExistsByIdResponse() {
        return new ProjectExistsByIdResponse();
    }

    @NotNull
    public ProjectFindAllRequest createProjectFindAllRequest() {
        return new ProjectFindAllRequest();
    }

    @NotNull
    public ProjectFindAllResponse createProjectFindAllResponse() {
        return new ProjectFindAllResponse();
    }

    @NotNull
    public ProjectSaveRequest createProjectSaveRequest() {
        return new ProjectSaveRequest();
    }

    @NotNull
    public ProjectSaveResponse createProjectSaveResponse() {
        return new ProjectSaveResponse();
    }

    @NotNull
    public TaskClearRequest createTaskClearRequest() {
        return new TaskClearRequest();
    }

    @NotNull
    public TaskClearResponse createTaskClearResponse() {
        return new TaskClearResponse();
    }

    @NotNull
    public TaskCountRequest createTaskCountRequest() {
        return new TaskCountRequest();
    }

    @NotNull
    public TaskCountResponse createTaskCountResponse() {
        return new TaskCountResponse();
    }

    @NotNull
    public TaskDeleteAllRequest createTaskDeleteAllRequest() {
        return new TaskDeleteAllRequest();
    }

    @NotNull
    public TaskDeleteAllResponse createTaskDeleteAllResponse() {
        return new TaskDeleteAllResponse();
    }

    @NotNull
    public TaskDeleteByIdRequest createTaskDeleteByIdRequest() {
        return new TaskDeleteByIdRequest();
    }

    @NotNull
    public TaskDeleteByIdResponse createTaskDeleteByIdResponse() {
        return new TaskDeleteByIdResponse();
    }

    @NotNull
    public TaskDeleteRequest createTaskDeleteRequest() {
        return new TaskDeleteRequest();
    }

    @NotNull
    public TaskDeleteResponse createTaskDeleteResponse() {
        return new TaskDeleteResponse();
    }

    @NotNull
    public TaskFindByIdRequest createTaskFindByIdRequest() {
        return new TaskFindByIdRequest();
    }

    @NotNull
    public TaskFindByIdResponse createTaskFindByIdResponse() {
        return new TaskFindByIdResponse();
    }

    @NotNull
    public TaskExistsByIdRequest createTaskExistsByIdRequest() {
        return new TaskExistsByIdRequest();
    }

    @NotNull
    public TaskExistsByIdResponse createTaskExistsByIdResponse() {
        return new TaskExistsByIdResponse();
    }

    @NotNull
    public TaskFindAllRequest createTaskFindAllRequest() {
        return new TaskFindAllRequest();
    }

    @NotNull
    public TaskFindAllResponse createTaskFindAllResponse() {
        return new TaskFindAllResponse();
    }

    @NotNull
    public TaskSaveRequest createTaskSaveRequest() {
        return new TaskSaveRequest();
    }

    @NotNull
    public TaskSaveResponse createTaskSaveResponse() {
        return new TaskSaveResponse();
    }

    @NotNull
    public AuthLoginRequest createAuthLoginRequest() {
        return new AuthLoginRequest();
    }

    @NotNull
    public AuthLoginResponse createAuthLoginResponse() {
        return new AuthLoginResponse();
    }

    @NotNull
    public AuthProfileRequest createAuthProfileRequest() {
        return new AuthProfileRequest();
    }

    @NotNull
    public AuthProfileResponse createAuthProfileResponse() {
        return new AuthProfileResponse();
    }

    @NotNull
    public AuthLogoutRequest createAuthLogoutRequest() {
        return new AuthLogoutRequest();
    }

    @NotNull
    public AuthLogoutResponse createAuthLogoutResponse() {
        return new AuthLogoutResponse();
    }

}

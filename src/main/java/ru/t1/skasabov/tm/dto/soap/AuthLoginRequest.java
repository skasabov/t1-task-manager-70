package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
        "login",
        "password"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "authLoginRequest")
public class AuthLoginRequest {

    @NotNull
    @XmlElement(required = true)
    protected String login;

    @NotNull
    @XmlElement(required = true)
    protected String password;

    public AuthLoginRequest(@NotNull final String login, @NotNull final String password) {
        this.login = login;
        this.password = password;
    }

}

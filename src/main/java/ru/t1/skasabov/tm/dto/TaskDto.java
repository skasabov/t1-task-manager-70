package ru.t1.skasabov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.skasabov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public class TaskDto extends AbstractModelDto {

    @NotNull
    @Column(nullable = false)
    private String name;

    @Nullable
    private String description;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(nullable = false, updatable = false)
    private Date created = new Date();

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "begin_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart = new Date();

    @Nullable
    @Column(name = "end_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private String userId;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public TaskDto(@NotNull final String name) {
        this.name = name;
    }

}

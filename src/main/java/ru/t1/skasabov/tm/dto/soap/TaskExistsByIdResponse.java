package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "exists"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskExistsByIdResponse")
public class TaskExistsByIdResponse {

    @NotNull
    @XmlElement(required = true)
    protected Boolean exists;

    public TaskExistsByIdResponse(@NotNull final Boolean exists) {
        this.exists = exists;
    }

}

package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "count"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectCountResponse")
public class ProjectCountResponse {

    @NotNull
    @XmlElement(name = "count")
    protected Long count;

    public ProjectCountResponse(@NotNull final Long count) {
        this.count = count;
    }

}

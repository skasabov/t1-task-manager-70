package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.UserDto;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
        "user"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "authProfileResponse")
public class AuthProfileResponse {

    @Nullable
    @XmlElement(name = "user")
    protected UserDto user;

    public AuthProfileResponse(@Nullable final UserDto user) {
        this.user = user;
    }

}

package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "project"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectDeleteRequest")
public class ProjectDeleteRequest {

    @NotNull
    @XmlElement(required = true)
    protected ProjectDto project;

    public ProjectDeleteRequest(@NotNull final ProjectDto project) {
        this.project = project;
    }

}

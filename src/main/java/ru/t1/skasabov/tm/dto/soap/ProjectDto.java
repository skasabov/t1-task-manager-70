package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectDto", propOrder = {
    "id",
    "created",
    "name",
    "description",
    "dateStart",
    "dateFinish",
    "status",
    "userId"
})
public class ProjectDto implements Serializable {

    @NotNull
    @XmlElement(required = true)
    protected String id = UUID.randomUUID().toString();

    @NotNull
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected Date created = new Date();

    @NotNull
    @XmlElement(required = true)
    protected String name;

    @Nullable
    @XmlElement(required = true)
    protected String description;

    @Nullable
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected Date dateStart = new Date();

    @Nullable
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected Date dateFinish;

    @NotNull
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected Status status = Status.NOT_STARTED;

    @NotNull
    @XmlElement(required = true)
    protected String userId;

}

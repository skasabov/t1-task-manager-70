package ru.t1.skasabov.tm.dto.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "status")
public enum Status {

    NOT_STARTED,
    IN_PROGRESS,
    COMPLETED;

    public String value() {
        return name();
    }

    public static Status fromValue(String v) {
        return valueOf(v);
    }

}

package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "id"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskDeleteByIdRequest")
public class TaskDeleteByIdRequest {

    @NotNull
    @XmlElement(required = true)
    protected String id;

    public TaskDeleteByIdRequest(@NotNull final String id) {
        this.id = id;
    }

}

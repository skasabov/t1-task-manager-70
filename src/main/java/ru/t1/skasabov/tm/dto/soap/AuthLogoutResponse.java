package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
        "result"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "authLogoutResponse")
public class AuthLogoutResponse {

    @NotNull
    @XmlElement(name = "result")
    protected Result result;

    public AuthLogoutResponse(@NotNull final Result result) {
        this.result = result;
    }

}

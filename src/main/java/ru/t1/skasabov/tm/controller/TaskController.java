package ru.t1.skasabov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.skasabov.tm.dto.CustomUser;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;

import java.util.Collection;

@Controller
public final class TaskController {

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @GetMapping("/task/create")
    public String create(@AuthenticationPrincipal @NotNull final CustomUser user) {
        @NotNull final TaskDto task = new TaskDto("New Task " + System.currentTimeMillis());
        task.setUserId(user.getUserId());
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") @NotNull final String id,
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) {
        taskRepository.deleteByUserIdAndId(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @NotNull
    @PostMapping("/task/edit/{task.id}")
    public String edit(
            @ModelAttribute("task") @NotNull final TaskDto task,
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) {
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) task.setProjectId(null);
        task.setUserId(user.getUserId());
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) {
        @Nullable final TaskDto task = taskRepository.findByUserIdAndId(user.getUserId(), id);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", getStatuses());
        modelAndView.addObject("projects", getProjects(user.getUserId()));
        return modelAndView;
    }

    @NotNull
    private Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    private Collection<ProjectDto> getProjects(@NotNull final String userId) {
        return projectRepository.findAllByUserId(userId);
    }

}

package ru.t1.skasabov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.skasabov.tm.dto.CustomUser;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;

@Controller
public final class TasksController {

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView index(@AuthenticationPrincipal @NotNull final CustomUser user) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task-list", "tasks", taskRepository.findAllByUserId(user.getUserId()));
        modelAndView.addObject("projectRepository", projectRepository);
        return modelAndView;
    }

}

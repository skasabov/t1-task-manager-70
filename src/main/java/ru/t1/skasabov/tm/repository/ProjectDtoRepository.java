package ru.t1.skasabov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.dto.ProjectDto;

import java.util.List;

public interface ProjectDtoRepository extends JpaRepository<ProjectDto, String> {

    @NotNull
    List<ProjectDto> findAllByUserId(@NotNull String userId);

    @NotNull
    ProjectDto findByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    @Transactional
    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Transactional
    void deleteAllByUserId(@NotNull String userId);

}

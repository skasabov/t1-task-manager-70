package ru.t1.skasabov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.dto.TaskDto;

import java.util.List;

public interface TaskDtoRepository extends JpaRepository<TaskDto, String> {

    @Transactional
    void deleteByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<TaskDto> findAllByUserId(@NotNull String userId);

    @NotNull
    TaskDto findByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    @Transactional
    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Transactional
    void deleteAllByUserId(@NotNull String userId);

}

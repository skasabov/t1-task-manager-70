package ru.t1.skasabov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.dto.CustomUser;
import ru.t1.skasabov.tm.enumerated.RoleType;
import ru.t1.skasabov.tm.model.Role;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Override
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {
        @Nullable final User user = userRepository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        @NotNull final List<Role> userRoles = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        for (@NotNull final Role role : userRoles) roles.add(role.toString());
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[] {}))
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMINISTRATOR);
        initUser("test", "test", RoleType.USER);
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
        return userRepository.findByLogin(login);
    }

    private void initUser(@NotNull final String login, @NotNull final String password, @NotNull final RoleType roleType) {
        @Nullable final User user = findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @Transactional
    public void createUser(@Nullable final String login, @Nullable final String password, @NotNull final RoleType roleType) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @NotNull final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

}

package ru.t1.skasabov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.Result;
import ru.t1.skasabov.tm.dto.UserDto;

public interface IAuthRestEndpoint {

    @NotNull
    Result login(@NotNull String login, @NotNull String password);

    @Nullable
    UserDto profile();

    @NotNull
    Result logout();

}

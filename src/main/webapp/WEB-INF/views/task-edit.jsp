<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>
<h1>TASK EDIT</h1>
<form:form action="/task/edit/${task.id}/" method="POST" modelAttribute="task">
    <form:input type="hidden" path="id"/>
    <form:input type="hidden" path="created"/>
    <p>
        <div>NAME:</div>
        <div><form:input type="text" required="required" path="name"/></div>
    </p>
    <p>
        <div>DESCRIPTION:</div>
        <div><form:input type="text" path="description"/></div>
    </p>
    <p>
        <div>PROJECT:</div>
        <form:select path="projectId">
            <form:option value="${null}" label="--- // ---" />
            <form:options items="${projects}" itemValue="id" itemLabel="name"/>
        </form:select>
    </p>
    <p>
        <div>STATUS:</div>
        <form:select path="status">
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </p>
    <p>
        <div>DATE START:</div>
        <div><form:input type="date" path="dateStart"/></div>
    </p>
    <p>
        <div>DATE FINISH:</div>
        <div><form:input type="date" path="dateFinish"/></div>
    </p>
    <button type="submit">SAVE TASK</button>
</form:form>
<jsp:include page="../include/_footer.jsp"/>
